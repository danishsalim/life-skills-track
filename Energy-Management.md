### Question 1: What are the activities you do that make you relax - Calm quadrant?

I go for walk for relaxing my mind.

### Question 2: When do you find getting into the Stress quadrant?

When i dont complete my work on time.

### Question 3: How do you understand if you are in the Excitement quadrant?

When I successfully complete a task, the inner satisfaction made me feel in Excitement quadrant.

### Question 4: Paraphrase the Sleep is your Superpower video in your own words in brief. Only the points, no explanation.

 - Lack of sleep can have significant negative impacts on health.
 - Sleep deprivation can lead to memory problems and increase the risk of cancer.
 - Sleep deprivation can impair brain function and reduce the brain’s ability to absorb new information.
 - improving sleep quality and prioritizing regular sleep can have numerous health benefits.
 - Regular sleep routines, including consistent bedtimes and wake-up times, can improve sleep quality and ensure a more restful night’s sleep.

### Question 5: What are some ideas that you can implement to sleep better?

 - Consistent bedtimes and wake-up times.
 - Avoiding caffeine after evening.
 

 ### Question 6: Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points, only the points.

 - Exercise has immediate positive effects on mood and focus.
 - Exercise protects the brain from conditions like depression and Alzheimer’s disease.
 - Regular exercise can improve memory and attention.
 - Exercise increases the volume of the hippocampus and prefrontal cortex.
 - Exercise can enhance cognitive function and protect against neurodegenerative diseases.

 

### Question 7: What are some steps you can take to exercise more?

 - Running or walking.
 - Using stairs. 