**Q 1: Which point(s) were new to you?**

 -  Some companies use tools like Trello, Jira for documentation. But if
   your team is not using any such tool, you should write down the
   requirements and share it with the team. This will help you get
   immediate feedback. Also, this will serve as a reference for future
   conversations
  -  Sometimes despite all efforts, the requirements are still vague. So during the implementation phase, get frequent feedback so that you are on track and everyone is on the same page
  -    Use the group chat/channels to communicate most of the time. This is preferable over private DMs simply because there are more eyes on the main channel.
  -   The way you ask a question determines whether it will be answered or not. You need to make it very easy for the person to answer your question. Messages like the database is not connecting, need your help or the build is not working, can you help me out, will not get you the answers you are looking for.
  -   Use Github gists to share code snippets. Use sandbox environments like Codepen, Codesandbox to share the entire setup

   

**Q 2: Which area do you think you need to improve on? What are your ideas to make progress in that area?**

I think I need to improve my questions asking area.

 - If there is any dought related to work then it should be clear immediately.
 - By  explaining the problem clearly, mentioning the solutions i tried out to fix the problem.
 - By asking the help for  good resources for fixing the problem.