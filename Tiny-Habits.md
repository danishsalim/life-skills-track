 **Question 1 : In this video, what was the most interesting story or idea for you?**

 I like the story of weight loss, How the speaker builds tiny habits to loss  his weight instead of seeking big changes quickly.


**Question 2 : How can you use B = MAP to make making new habits easier? What are M, A and P.**

The formula B=MAP (Behavior equals Motivation plus Ability plus Prompt) explains that motivation, ability, and prompt are key factors in developing new habits. By shrinking the habit to a tiny version and celebrating small wins, I can develop new habits.

**Question 3 : Why it is important to "Shine" or Celebrate after each successful completion of habit? (This is the most important concept in today's topic. Whatever you celebrate becomes a habit)**

Celebrating small wins after completing tiny habits generates a feeling of success and boosts motivation. This success momentum leads to sustained habit growth and the ability to take on more challenging behaviors. Celebrating small wins creates a positive feedback loop, reinforcing the habit and increasing motivation to continue.


**Question 4 : In this video, what was the most interesting story or idea for you?**

The speaker shares the story of a photography professor named Jerry. At the beginning of semester, he split his students into two groups: one group was graded on the quantity of their work, while the other group was graded on the quality. Surprisingly, at the end of the semester, the group graded on quantity produced better work than the group graded on quality. This story emphasizes the importance of consistent practice and repetition in improving skills and achieving success.


**Question 5 :  What is the book's perspective about Identity?**

Identity change is the North Star of  habit change. Shifting our identity to align with desired habits is the ultimate form of intrinsic motivation.

**Question 6 : Write about the book's perspective on how to make a habit easier to do?** 

The book's perspective on how to make a habit easier to do is based on four laws of behavior change. Make habits :

 - obvious
 - attractive
 - easy
 - satisfying 



**Question 7 :  Write about the book's perspective on how to make a habit harder to do?**

The book's perspective on how to make a habit harder :
 - by making cue invisible
 - by making action unattractive
 - reward unsatisfying

**Question 8 : Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?**

After dinner walking 1000 foot-stepsc. Steps for building this habit:

 - start with one hundred foot-steps
 - gradually increasing the foot-steps
 - feeling haapy after walking each day


 

**Question 9 : Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?**

Scrolling reels before sleeping. The steps I would take :

 - keep my phone away during sleep time
 - start reading some good books before sleeping
