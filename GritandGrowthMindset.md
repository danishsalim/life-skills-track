 **Question 1 : Paraphrase (summarize) the video in a few (1 or 2) lines. Use your own words.**

 In the video, Speaker discusses the importance of "grit" in achieving success, which she defines as passion and perseverance for very long-term goals.

**Question 2 : Paraphrase (summarize) the video in a few (1 or 2) lines in your own words.**

In the video, Speaker discuss about Fixed mindset and Growth mindset. Fixed mindset believes skills and intelligence are set and cannot be changed while Growth mindset believes skills and intelligence can be developed through effort and learning.

**Question 3 : What is the Internal Locus of Control? What is the key point in the video?**

Locus of Control means the degree to which you believe you have control over your life. An internal locus of control is the belief that success comes from personal effort and **hard work**. The key point is that one should build the belief that he can control his destiny with hard work.

**Question 4 : What are the key points mentioned by speaker to build growth mindset (explanation not needed).**
The key points are:

 - Believe in your ability to figure things out
 - Question your assumptions
 - Create a lifelong learning curriculum
 - Honor the struggle

**Question 5 : What are your ideas to take action and build Growth Mindset?**
My ideas are :

 - Hard work 
 - Patience
 - Accept and learn from failure