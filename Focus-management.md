**Q 1: What is Deep Work?**

Deep work is the term for focused work without distractions on challenging tasks.

**Q2: According to author how to do deep work properly, in a few points?**

 - Undistracted concentration.
 - allowing for focused on a task.
 - The optimal duration for deep work is at least 1 hour.
 
**Q3: How can you implement the principles in your day to day life?**

I can implement the principles by doing my work with focus and undistracted state.

**Q4: What are the dangers of social media, in brief?**
 - Social media is addictive.
 - Can lead to loneliness, anxiety and mental health issues.
 - Reduce concentration.
 - Reduce productiveness and peace.