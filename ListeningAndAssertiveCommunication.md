# Listening and Active Communication

**Question 1 : What are the steps/strategies to do Active Listening?** 

 -   Focus on the speaker and the topic.
 - Avoiding interruptions and allowing them to finish before responding.
 -   Use "door openers," phrases that show interest and encourage the speaker to continue talking.
 -   Demonstrate active listening through body language.
 - Taking notes if necessary.
 -   Paraphrase what others have said to ensure mutual understanding.

 **Question 2 : According to Fisher's model, what are the key points of Reflective Listening?** 

According to Fisher's model

 - Listener should listen more and talk less
 - Listener should respond to things the other person says about him- or herself rather than about other people or situations.
 - Listener should try to respond to feelings, not just to content.
 - The listener can be most in touch with the other's frame of reference by responding to feelings that are expressed rather than unexpressed.
 - The listener should respond with acceptance and empathy not with fake concerns.
 
 **Question 3 : What are the obstacles in your listening process?**

One of the obstacle in my listening process is my inner thoughts during listening.

**Question 4 : What can you do to improve your listening?**

I can improve my listening by controlling my inner thoughts and focussing the topic .

**Question 5 : When do you switch to Passive communication style in your day to day life??**

Usually i dont switch to passive communication style but in some cases
when i see the need of other people is more important than mine then i switch to passive communication.

**Question 6 : When do you switch into Aggressive communication styles in your day to day life?**

When I am doing an important task and someone interrupts me again and again , then i switch to Agressive communication styles.

**Question 7 : When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?**

I personally use assertive communication style and avoid Passive Aggressive communication styles , but there may be situation where I may use Passive Aggresive communication style like when I perceive unfair treatment from someone close to me.

**Question 8 : How can you make your communication assertive? You can watch and analyse the videos, then think what would be a few steps you can apply in your own life**

I can make my communication assertive by expressing my needs while also respecting the needs of others and by building sustainable and mutually satisfying relationships.

