## Apache Kafka

Modern cloud applications require a real-time experience

Apache Kafka is an open-source, distributed streaming platform for real-time event-driven applications.

**Key Characteristics of Apache Kafka:**

- Distributed and runs across multiple servers
- Allows for high volume of users without performance lag
- Maintains high accuracy and order of data records

**Use Cases for Apache Kafka:**

- Decoupling system dependencies
- Messaging
- Location tracking
  - Example: ride share service tracking driver locations
- Data gathering
  - Example: music application collecting user data for  recommendations

**How Apache Kafka Works:**

Built on four core APIs:
- Producer API: produces streams of data to topics
- Consumer API: subscribes to topics and ingests data
- Streams API: consumes, analyzes, and transforms data in real time
- Connector API:integrating data sources

**Conclusion:**

Apache Kafka is a very powerful platform that enables real-time experiences in modern 
cloud based application.

**references:**

* https://youtu.be/06iRM1Ghr1k?si=DTA7bLE2So5XxBhz
* https://youtu.be/aj9CDZm0Glc?si=Ikm-RGWhP8zYUC21