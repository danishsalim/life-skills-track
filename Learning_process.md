# Learning Process
 **Question 1 :  What is the Feynman Technique? Explain in 1 line.**
 
  The Feynman Technique is a study method named after the physicist Richard    Feyman .It involves explaining a concept as if you were teaching it to someone else, in simple and plain language.
 
**The process consists of four steps:**
 - Write down the name of the concept.
 - Explain the concept in simple terms, using examples. 
 - Identify areas where your understanding is shaky and review them. 
 - Look for technical terms or complicated language and simplify them.
 
 **Question 2 : In this video, what was the most interesting story or idea for you?**
 
 In this video the most interesting story for me is the story of **Salvador Dali** . Whenever  Salvador Dali stuck in any problem he used to relaxed his mind by sitting on a chair taking keys on his hand relaxing and capturing ideas as he relaxed so much that he fell to sleep the keys from his hand fell down and the clatter woke him up and his mind take those ideas  from diffuse mode to focused mode so that he can refine them and use them in his painting.

 **Question 3 :  What are active and diffused modes of thinking?**
 
 **Active mode :** Active modes of thinking involves concentrated thinking on familiar concepts or patterns.Thoughts move smoothly along established pathways.
 
 **Diffused mode :** Diffused mode involves around relaxed mind states. It allows for wide-ranging thinking and exploration of new ideas.

 **Question 4 : According to the video, what are the steps to take when approaching a new topic? Only mention the points.?**
 
 The steps are:

 - Deconstruct the topic
 - Learn enough to self-correct
 - Remove practice barriers
 - Practice atleast 20 hours

 **Question 5 :What are some of the actions you can take going forward to improve your learning process.?**
 
 Some of the actions that I can take for going forward to improve my learning process are:

 -  By focussing on the concept. Approaching it with curiosity and joy.
 -  By making the process of learning fun and easy.
 -  By making the concept myself.
 -  By  teaching new concepts to others
