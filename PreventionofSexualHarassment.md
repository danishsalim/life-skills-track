#  Prevention of Sexual Harassment

**Question 1 : -   What kinds of behaviour cause sexual harassment??** 

There are three forms of sexual harassment behavior: 

 - Verbal
 - Visual
 - Physical
 
**Verbal :** This includes comments about a person's body, sexual and gender based jokes or threats.

**Visual :** This can includes unwelcome posters, obscene pictures and videos.

  **Physical :** This includes sexual assault, inappropriate touching such as kissing, hugging, stroking or rubbing.

 **Question 2 : - What would you do in case you face or witness any incident or repeated incidents of such behaviour?**

If I face or witness any incident of such behaviour then first I will assertively tell the person to stop. If the person doesnot stop I will report a complaint against the person in anti-sexual-harassment department.
